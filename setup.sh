
gcloud organizations list
gcloud beta billing accounts list

TF_VAR_billing_account=$(gcloud beta billing accounts list --filter=open=true --format="value(name)" --limit=1)
TF_VAR_billing_account=${TF_VAR_billing_account//[[:blank:]]/}

#export TF_VAR_org_id=YOUR_ORG_ID
export TF_VAR_billing_account=YOUR_BILLING_ACCOUNT_ID
export TF_ADMIN=${USER}-terraform-admin-1227
export TF_CREDS=~/.config/gcloud/${USER}-terraform-admin.json


echo "============================="
echo "Create the Terraform Admin Project"
echo "============================="
gcloud projects create ${TF_ADMIN} \
  --set-as-default

gcloud beta billing projects link ${TF_ADMIN} \
  --billing-account 013A03-224109-0B536B
#${TF_VAR_billing_account}


echo "============================="
echo "Create the Terraform service account"
echo "============================="
gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_CREDS} \
  --iam-account terraform@${TF_ADMIN}.iam.gserviceaccount.com
  
gcloud projects add-iam-policy-binding ${TF_ADMIN} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/viewer

gcloud projects add-iam-policy-binding ${TF_ADMIN} \
  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
  --role roles/storage.admin

gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable serviceusage.googleapis.com


echo "============================="
echo "Add project-level permissions"
echo "============================="
#gcloud projects add-iam-policy-binding ${TF_ADMIN} \
#  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
#  --role roles/resourcemanager.projectCreator

#gcloud projects add-iam-policy-binding ${TF_ADMIN} \
#  --member serviceAccount:terraform@${TF_ADMIN}.iam.gserviceaccount.com \
#  --role roles/billing.user
  

echo "============================="
echo "Set up the remote Cloud Storage"
echo "============================="
gsutil mb -p ${TF_ADMIN} gs://${TF_ADMIN}

cat > backend.tf << EOF
terraform {
 backend "gcs" {
   bucket  = "${TF_ADMIN}"
   prefix  = "terraform/state"
 }
}
EOF

gsutil versioning set on gs://${TF_ADMIN}

export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}


export TF_VAR_project_name=${USER}-test-compute-opendata
export TF_VAR_region=europe-west2

terraform init
terraform plan
terraform apply
